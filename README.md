# Resurance

On-demand insurance policy changes within a second.
See https://devpost.com/software/resurance

Implemented as aniversal (nee isomporhic) Clojurescript React app for mobile and/or web. Runs on Docker and Heroku using node express, bootstrap, and material design, with hotloaded code shared between frontend and backend.

## References

https://clojure.org/
https://clojurescript.org/
https://reagent-project.github.io/
https://getbootstrap.com/docs/4.0/layout/overview/
http://www.material-ui.com
https://expressjs.com/

## Deploy with Docker

Start a local web server in a Docker container:

    docker-compose up release

Access http://localhost:5000 from a browser.

## Deploy to Heroku

To start a server on Heroku:

    heroku apps:create
    git push heroku master
    heroku open

This will open the site in your browser.

## License

Copyright © 2018 Terje Norderhaug

Distributed under the Eclipse Public License either version 1.0
or (at your option) any later version.
