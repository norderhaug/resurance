(ns app.session
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [<! chan]]
   [reagent.core :as reagent]
   [re-frame.core :as rf
    :refer [reg-sub]]
   [util.rflib :as rflib
    :refer [reg-property]]
   #_[re-frame.http-fx]
   [taoensso.timbre :as timbre]
   [app.policy :as policy]
   [app.request :as request]))

(defn solartis-shape [detail]
  {:property {0 {:property-detail detail}}})

(defn sync-solartis [{:as detail} & [init?]]
  {:pre [(map? detail)]}
  (let [shape (solartis-shape detail)]
    (timbre/debug "Sync Solartis:" shape init?)
    (go-loop [data (<! (request/fetch-solartis shape))]
      (timbre/debug "Got Solartis:" data)
      (if (map? data)
        (do (rf/dispatch [:dashboard :renters data])
            (when init?
              (rf/dispatch [:dashboard :original data])))
        (timbre/error "Solartis Sync failed for:" shape)))))

(defn init-solartis []
  (rf/dispatch
   [:solartis/init
    (->> policy/insurance-properties
         (map (fn [[k v]]
                (vector k (rand-nth (:options v)))))
         (into {}))]))

(def interceptors [#_(when ^boolean js/goog.DEBUG debug)
                   rf/trim-v])

(defn state [initial]
  (->> initial
       (map #(vector (first %)(reagent/atom (second %))))
       (into {})))

(defn subscriptions [ks]
  (into {} (map #(vector % (rf/subscribe [%])) ks)))

(defn initialize [initial]

  (rf/reg-event-db
   :initialize
   (fn [db _] initial))

  (rf/reg-event-db
   :update
   (fn [db [_ path f]]
     (update-in db path f)))

  (rf/reg-event-db
   :assign
   (fn [db [_ path value]]
     (timbre/debug "Assign:" path value)
     (assoc-in db path value)))

  (reg-property :brand)
  (reg-property :mode)
  (reg-property :tab)
  (reg-property
   :change-tab
   {:dispatch (fn [_ tab] [:tab :current tab])
    :pubnub/publish (fn [_ tab]
                      {:channel "demo" :message {:tab tab}})})
  (reg-property :stage)
  (reg-property :mobile)
  (reg-property :dashboard)

  (rf/reg-event-db
   :solartis/sync
   (fn [db [_ preference & [reset?]]]
     (sync-solartis (or preference
                       (get-in db [:dashboard :preference]))
                    reset?)
     db))

  (rf/reg-event-db
   :solartis/preference
   (fn [db [_ {:as props} & [reset?]]]
     (timbre/debug "Solartis set preference:" props reset?)
     (let [db (update-in db [:dashboard :preference] merge props)
           preference (get-in db [:dashboard :preference])]
       (rf/dispatch [:solartis/sync preference reset?])
       db)))

  (rf/reg-event-db
   :solartis/init
   (fn [db [_ {:as props}]]
     (timbre/debug "Solartis init:" props)
     (let [db (assoc-in db [:dashboard :initial] props)]
       (rf/dispatch [:solartis/preference props true])
       db)))

  (rf/reg-event-db
   :solartis/update-policy
   (fn [db [_]]
     (timbre/debug "Update Policy")
     (rf/dispatch [:solartis/init (get-in db [:dashboard :preference])])
     (assoc-in db [:mobile :show-completion] true)))

  (rf/dispatch-sync [:initialize])
  (init-solartis))
