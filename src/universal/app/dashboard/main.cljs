(ns app.dashboard.main
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
     :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [util.lib :as lib]
   [app.policy :as policy]
   [app.dashboard.pane
    :refer [pane]]))

(defn update-solartis [name value]
  (rf/dispatch [:solartis/sync
                {:property
                 {0
                  {:property-detail
                   {name value}}}}]))

(defn insurance-form-group [{:keys [session tag label]}]
 (let [value (get-in @(:dashboard session) [:preference tag])]
  [:div.form-group
   [:label (or label (get-in policy/insurance-properties [tag :label]))]
   [:select.form-control
    {:on-change #(update-solartis tag (-> % .-target .-value))}
    (for [item (get-in policy/insurance-properties [tag :options])]
      ^{:key (str item)}
      [:option
       {:selected (if (= value (str item)) true)}
       (str item)])]]))

(defn form-card [session]
  [:div.card
   [:div.card-header
    [:div.card-title "Plan Options"]]
   [:div.card-body
    [:form
     [insurance-form-group
      {:session session :tag :deductible}]
     [insurance-form-group
      {:session session :label "Property Limit" :tag :personal-property-limit}]
     [insurance-form-group
      {:session session :label "Liability Limit" :tag :personal-liabilty-limit}]
     [insurance-form-group
      {:session session :tag :medical-payment-limit}]]]])

(defn view [{:keys [dashboard] :as session}]
  (let [dashboard @dashboard
        renters (:renters dashboard)
        {:keys [total-annual-premium totalmonthly-premium policy-expiration-date
                carrier product-name policy-effective-date minimum-deductible]
         :as renters-property}
        (get-in renters [:renters-property])]
    [:div
     [:div.card-deck
      [form-card session]
      [:div.card
       [:div.card-header
        [:div.card-title "Current Policy"]]
       [:div.card-body
        [:table.table
         (into [:tbody]
               (for [[k title] {:total-annual-premium "Annual Premium"
                                :totalmonthly-premium "Monthly Premium"
                                :policy-effective-date "Effective"
                                :policy-expiration-date "Expires"
                                :carrier "Carrier"
                                :product-name "Product"
                                :minimum-deductible "Minimum Deductible"}]

                 [:tr [:th title][:td (k renters-property)]]))]]]]]))
