(ns app.request
  "Query the server API for data"
  (:require-macros
   [cljs.core.async.macros
    :refer [go go-loop]])
  (:require
   [cljs.core.async :as async
    :refer [<! chan]]
   [taoensso.timbre :as timbre]
   [util.chan :as chan]
   [camel-snake-kebab.core
    :refer [->kebab-case-keyword]]
   [camel-snake-kebab.extras
    :refer [transform-keys]]
   [cljs-http.client :as http]))

(defn using-cljs-keys [data]
  (transform-keys ->kebab-case-keyword data))

(def default-renters-property
  {:property
   {0
    {:property-detail
     {:zip-code "43220"}}}})

#_
(transform-solartis {:HelloWorld {"SubLevel" "FooBar"}})

(defn fetch-solartis [& [query]]
  (http/post "/solartis"
            {:channel (chan 1 (map (comp using-cljs-keys :body)))
             :json-params (or query default-renters-property)}))

#_
(chan/echo (fetch-solartis {:deductible "1000"}))
