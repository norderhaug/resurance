(ns app.mobile.update
  (:require
   [reagent.core :as reagent
    :refer [atom]]
   [reagent.format
    :refer [currency-format]]
   [re-frame.core :as rf]
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :as material
    :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [taoensso.timbre :as timbre]
   [goog.string :as gstring]
   [goog.string.format]
   [app.mobile.pane
     :refer [pane]]
   [app.policy :as policy]))

; https://v0.material-ui.com/#/components/app-bar

(defn radio-selector [session]
  (let [{:keys [options] :as config}
        (:deductible policy/insurance-properties)]
    (into [:div]
          (for [option options]
            [ui/radio-button
             {:value (str option)}]))))

(defn currency-compact [n]
  (let [s (currency-format n)]
    (if (= ".00" (subs s (- (count s) 3)))
      (subs s 0 (- (count s) 3))
      s)))

(defn simple-currency-selector [{:keys [label value options on-change]}]
  (into
     [ui/select-field
      {:floating-label-text label
       :style {:margin-right "1em" :width "8em"}
       :on-change on-change
       :value value}]
     (for [item options]
       [ui/menu-item
        {:primary-text (currency-compact item)
         :value item}])))

(defn policy-choice-selector [{:keys [session tag]}]
  (let [{:keys [label options]}
        (get policy/insurance-properties tag)
        value (get-in @(:dashboard session)
                      [:preference tag]
                      (first options))
        on-change (fn [event index value]
                    (timbre/debug "CHANGE:" value)
                    (rf/dispatch [:solartis/preference {tag value}]))]
    [simple-currency-selector
     {:label label
      :on-change on-change
      :value value
      :options options}]))

(defn plan-card [session]
  [ui/card {:style {:margin-top "0.5em"
                    :margin-left "0.5em"
                    :margin-right "0.5em"}}
   #_[ui/card-title "Plan"]
   [ui/card-header
    {:title "Plan Options"
     :subtitle "Customize your insurance"}]
   [ui/card-media {:style {:margin-left "1em"
                           :margin-right "1em"}}
    #_[radio-selector session]
    [policy-choice-selector
     {:session session :tag :deductible}]
    [policy-choice-selector
     {:session session :tag :personal-property-limit}]
    [policy-choice-selector
     {:session session :tag :personal-liabilty-limit}]
    [policy-choice-selector
     {:session session :tag :medical-payment-limit}]]])

(defn policy-card [{:keys [dashboard] :as session}]
  (let [{:keys [renters preference initial original]} @dashboard
        props (policy/properties renters)
        original-props (policy/properties original)
        changed? (not= preference initial)]
    (timbre/debug "org" original)
    [ui/card {:style {:margin-top "0.5em"
                      :margin-left "0.5em"
                      :margin-right "0.5em"
                      :margin-bottom "0.5em"}}
     [ui/card-header
      {:title "Policy Changes"
       :subtitle "Effect on your payments"}]
     [:div {:style {:margin-left "0.5em "
                    :margin-right "0.5em "}}; ui/card-text
      [ui/table {:selectable false}
       (into [ui/table-body {:display-row-checkbox false}]
             (for [p [:total-annual-premium :totalmonthly-premium]
                   :let [{:keys [label value] :as prop}
                         (get props p)]]
               [ui/table-row {:selectable false}
                [ui/table-row-column
                 (or label "???")]
                [ui/table-row-column
                 [ui/chip (if value (currency-format value) "...")]]
                [ui/table-row-column
                 (let [org (get original-props p)]
                    (if (and value (:value org))
                      (let [diff (- (js/parseFloat value)
                                    (js/parseFloat (:value org)))]
                        (if (not= 0 diff)
                          [ui/chip {:label-color "white"
                                    :background-color
                                    (if (neg? diff) "green" "red" )}
                           (str
                            (if (pos? diff) "+")
                            (currency-format diff))]))))]]))]]
     [ui/card-actions
      [ui/raised-button
       {:label "Update Policy"
        :disabled (not changed?)
        :on-click #(rf/dispatch [:solartis/update-policy])
        :primary true
        :full-width true}]]]))

(defn completion-dialog [{:keys [dashboard mobile] :as session}]
  (let [{:keys [renters preference initial original]} @dashboard
        {:keys [totalmonthly-premium] :as props} (policy/properties renters)]
    [ui/dialog {:title "Policy Updated"
                :open (get-in @mobile [:show-completion])
                :on-request-close #(rf/dispatch [:mobile :show-completion nil])}
     [:p "Your renters insurance has been updated."
      (if-let [value (:value totalmonthly-premium)]
        (str "The new monthly premium is "
            (currency-format value)))]
     [:p "Did you know: You can save up to $12/month by improving your credit score."]]))

(defmethod pane ["update"] [session]
  [:div {:style {:font-size "larger"}}
   [plan-card session]
   [policy-card session]
   [completion-dialog session]])
