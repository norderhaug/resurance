(ns app.policy
  "Define renters unsurance policy options"
  (:require
   [goog.string :as gstring]
   [reagent.core :as reagent
     :refer [atom]]
   [util.lib :as lib]))

(defn properties [policy]
  (let [{:keys [total-annual-premium totalmonthly-premium policy-expiration-date
                carrier product-name policy-effective-date minimum-deductible]
         :as renters-property}
        (get-in policy [:renters-property])]
    {:total-annual-premium
     {:label "Annual Premium"
      :value total-annual-premium}
     :totalmonthly-premium
     {:label "Monthly Premium"
      :value totalmonthly-premium}}))

(defn strs [& items]
  (map str items))

(def insurance-properties
  {:personal-property-limit
   {:label "Property Limit"
    :options
    (strs 5000, 10000, 20000, 30000, 40000, 50000, 60000, 70000,
          80000, 90000, 100000, 110000, 120000, 130000, 140000,
          150000, 160000, 170000, 180000, 190000, 200000, 210000,
          220000, 230000, 240000, 250000)}
   :personal-liabilty-limit
   {:label "Liability Limit"
    :options
    (strs 100000, 200000, 300000, 500000, 1000000)}
   :medical-payment-limit
   {:label "Medical Pay Limit"
    :options
    (strs 1000, 2000, 3000, 4000, 5000)}
   :deductible
   {:label "Deductible"
    :options
    (strs 250, 500, 1000, 2500)}})
