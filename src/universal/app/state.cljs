(ns app.state
  (:require
   [camel-snake-kebab.core
    :refer [->kebab-case-keyword]]
   [camel-snake-kebab.extras
    :refer [transform-keys]]))

(defn transform-kebab-keys [m]
  (transform-keys ->kebab-case-keyword m))

(def state
  {:brand "Resurance"
   :mode {:current "mobile"
          :options [{:id "split" :title "Split"}
                    {:id "mobile" :title "Mobile"}
                    {:id "dashboard" :title "Dashboard"}]}
   :stage nil
   :dashboard {}
   :mobile {:stage "update"}
   :tab {:current "about"
         :options [{:id "about" :title "About"}
                   {:id "main" :title "Dashboard"}
                   {:id "info" :title "Data"}]}})
