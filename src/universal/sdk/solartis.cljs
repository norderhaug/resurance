(ns sdk.solartis
  (:require-macros
   [cljs.core.async.macros :refer [go go-loop]])
  (:require
   [cljs-http.client :as http]
   [cljs.core.async :refer [<! chan]]
   [camel-snake-kebab.core
    :refer [->kebab-case-keyword ->PascalCaseString]]
   [camel-snake-kebab.extras
    :refer [transform-keys]]
   [util.lib :as lib
    :refer [deep-merge]]
   [util.chan :as chan]))

(defn service-headers [service]
  (merge
   (:headers service)
   {"Token" (:token service)
    "Accept" "application/json"
    "Content-Type" "application/json"}))

(defn fetch-json [service payload]
  (http/post (:endpoint service)
             {:headers (service-headers service)
              :with-credentials? false
              :json-params payload})) ;; rating request

(defn encode-token [& ss]
  (clojure.string/join (map #(clojure.string/replace % #"\s" "") ss)))

(defn solartis-keys [data]
  (transform-keys #(if (number? %) % (->PascalCaseString %)) data))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LEISURE TRAVEL

(def leisure-travel-service
  {:endpoint "https://travelapihk.solartis.net/DroolsV4_2/DroolsService/FireEventV2"
   :access #{:rate :create-customer :issue-policy}
   :headers {"EventName" "InvokeRatingV2"}
   :token
   (encode-token
        "4hBNmCbubX10dn0Ntx+0dhcL2h/w5px8qVY4FYnOun6fO7GzhrjuomRPMKx/9xYHkVM0lw7VYr4yZor"
        "/AyFKkvpSQzcv8vvlIyYVCE1yy44kYsWgEB0vgCtUqbG3NdZchXorckgEpopxll5iWq1Sx22X6lqBc725RrGj"
        "Asy+9eTQCE+1FLWAFzFcv17hdsk9czuXi9azAdd+gx6eY3Dq5PhfjA21pwEiaqeG8DV8EFmqt+6lxKf/fuvz"
        "qZILi98g882sAQHRKd9OQpvqO9NoAQ2nQB12/PXPYKbm0/qCkloaUucv3BejhEe0m3eNSWy09Zc/yDr"
        "69gUSZRhYlbYnNIyouiTt1M9RaVNVLUFCZAZhWZcmcH+SKkTAGF9ntacvIkscajZL5XnMZE/RrGPt4bwM"
        "SwUnu3tAyr4vM25PtTILOKlBvKouGrlS/kOHlnC4Pg4xLGi7892Vy8uMC7Ou6f18c0grvB8uXbAmnvpCS"
        "OrhJkGdLzHpQ3RG3mWPkYe+WB8tr5UrB44ZoqljwzYnjIM6GPxWhvO4RMhk2Fb/x7YwZ8TrPAQjPvYC"
        "zlB3xdoJsPLbMpgi5Mfmm0OKKOFlMiCafIlfrtVo1S08FIo8YrNQMjPQ+aQuHvlWG3jKSE5LWqf3/xYW9R"
        "Q8D6vhdzJ4cDAFEo5TyvmJJ5KBuvYAOGF9yI+WDHPNl+XVc7CUenCxBZHLnv9JOmdHS4wYe9ohE96T5"
        "lv+d+ogncvNWnZ5WKbBL6d2b532kJBtxc+V7xLcPQMUG2xAXa6ZQttgWU+OWwjpxuCysIutH8HW6XT"
        "P2Y5kLGnOlTRJn2ru4fvMLU/6qKWLrbneAPZeb//NH9MpcrV/as/pPm2UnbhSIso1qySyEdJLRovexbqYn"
        "tysXT/yTyNaNU89I2CQaopdT0IPD6sip800gYJ02JiDEZWhNXD1K11Q5L2x1nYqQQeDMEnOngq7tOddf"
        "Ez4y/tURqDA2g==")})

(def leisure-travel-payload-sample
  {"ServiceRequestDetail" { "ServiceRequestVersion" "1.0",
                            "ServiceResponseVersion" "1.0",
                            "OwnerId" "15",
                            "ResponseType" "JSON",
                            "RegionCode" "US",
                            "Token" "iMgdpnp436QjuYQ2tn9HU+SgJDsyFW8kDSoLHGwadQKpl/wr4yKoVMjkSfJB/FKqjFqrLmebBUqmHM/dVAMv+DDPaXOt/GY7mjWZXGT2z0ArRvyiojuQz8LrhVYihPok4SNftJWQa7rNrqjh5PWJcVbnbLVHo4e9WNp/4X9Sai/5LrKZ1J99H71yIi/9rXsAKhCk5lGzFaO3SXLwJluxpsUM+5MiljDi4NFM+/K0KPeUOuL8R2FZD9RobXBVvR7xPdH1oZ71jBgOmg59/75ugVgJ/35PRXWc1Emi9SPqQHHEmsD4AoH2xj45UGMY3hjx0sz/uABSHKxNwA8DwPTBS7jaN2XqKLCayPsSWHVH04XW+ZeDaw1iRLb/0euQfXT97fiPQgPQSjtDvxif6cFXZQX/BHYIcAUjZtdcuv098quieEPaIWri6DUFEIzf/uRM7R4itGHIU7/B0cMwwQNFhxjtMVAgSBq7Kw4a8kf5s+2XOip5XV3KeT5U4u8HX6/xdKxhpP8gLng6PoGj0MEnSugWYicqO81QJH2UISAaI3iDgNxzCSFmsAChwURz5g/RRmHqLknkxHh4lvFfjQGlTIfdNwW+CwHbg0EQFt/c/4M6CofYIAQVzElcVRih/j88U7EqGr8v1mrTPel1sBXl+auuXSlODNRoeDVogcqA1Fr9504oM1kbCEmrVIqjUxUmK9a8CWsZEjIYiNuY0KncVKuuXSlODNRoeDVogcqA1Foa9LkTsAlC3ItSy+gbt0NdLaJdXbWS8ClBkbddTRCbeWRNMZNDq/vS1JTb8FKCl5OsniK5MZQnMbZdqNHlqbv2WOw2j+jUU0BThnDgEX6XEjSt7SoS1Xd6+F4DyPRZsQbD7XGhOWwTgbKALHDL6LviA3K4dHjVKHv1xOd8L284LsWKpOWqHf8xYqRIiCHZgQEtSRZCdfQPbklkXKGNaTjD",
                            "UserName" "travelagent",
                            "LanguageCode" "en"}

   "QuoteInformation" { "ProductID" "619",
                        "ProductVerID" "706",
                        "ProductNumber" "ILT",
                        "ProductVerNumber" "1.0",
                        "ProducerCode" "86201",
                        "OwnerId" "15",
                        "PlanName" "Air Ticket Protector",
                        "PlanCode" "1",
                        "DepartureDate" "06/25/2017",
                        "ReturnDate" "06/31/2017",
                        "DepositDate" "06/03/2017",
                        "DestinationCountry" "France",
                        "PolicyEffectiveDate" "06/25/2017",
                        "RentalStartDate"  "06/25/2017",
                        "RentalEndDate"  "06/31/2017",
                        "RentalLimit"  "35000",
                        "NumberOfRentalCars"  "1",
                        "TripCancellationCoverage" "With Trip Cancellation",
                        "StateCode" "GA",
                        "QuoteType" "New Business",
                        "EventName" "InvokeRatingV2",
                        "TravelerList" [ {"TravelerDOB" "02/14/1990",
                                          "TravelCost" "2500"}]}})


(defn fetch-leisure-travel []
  (fetch-json leisure-travel-service
              leisure-travel-payload-sample))

(defn test-leisure-travel []
  (-> (fetch-leisure-travel)
      (util.chan/echo)))

#_
(test-leisure-travel)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PROFESSONAL LIABILTY

(def professional-liability-service
  {:endpoint "https://profapihk.solartis.net/DroolsV6/DroolsService/FireEventV2"
   :access #{:rate}
   :headers {"EventName" "PLICalculateRate"
             "EventVersion" "2.2.2.1"}
   :token "LzOEfkD2G3fGNoAE/2OgpmZ6ZAWRLLmX3FtnciU1JdwB7qwRQYgWqXWRBQy4Jisd12JrkD8zgu8i1v6CrW16dewQ2Scv8nR12BrVp+lvGg0lxsyIucmAQER65neynmkI26Y5kbAKryCz/35ZriwlEfDXZLd3QYKRS9sc1TFkEt6yTSAaP1ldfyjTLdzzfEQFoh7wOZWhBG29a4ODeWgWKNqFuUXerFyeXwvtro+fUBYQXd5mtYN/lzWohbRL16+TihHcVST4ZQnE4Fonl8CEhQ6YFdByTdos0eHQxT/dz4891JftAgkDVq1rOak9nOy0qKAb6NoYtEXSPkZxG/nyasMGl47fu6XYWahaa/QIZENFrZ4dBGnlmva/Hik80iY7SwSCRT42VVmyhq6ysvWlyFyi4P+62OAQH7p5AoUM0n5fdi07EU6YqwSTUzWil4s3LplkIHDT7RPe+14gjvaPO1LxHFpvJF+c4svSQ7/R1nMH2HCR8o6y8vDoTDdkYe5HZ5EshW3rZCpHonG2vmGxPu2gVz/SRhNP5ODPqs88n13IswG5c0y1cYIWWoc/vdLvDUrSqf4AQQE31LyBlGGTg6uPCY4eYWCcrNtoQ2MK535MAkok7QvW9MFsv6mwc/nJvAxLBSe7e0DKvi8zbk+1Mgs4qUG8qi4auVL+Q4eWcLg+DjEsaLvz3ZXLy4wLs67p/XxzSCu8Hy5dsCae+kJI6uEmQZ0vMelDdEbeZY+Rh75YHy2vlSsHjhmiqWPDNieMgzoY/FaG87hEyGTYVv/HtjBnxOs8BCM+9gLOUHfF2gmw8tsymCLkx+abQ4oo4WUyIJp8iV+u1WjVLTwUijxis1AyM9D5pC4e+VYbeMpITktap/f/Fhb1FDwPq+F3MnhwMAUSjlPK+YknkoG69gA4YX3Ij5YMc82X5dVzsJR6cLEFkcue/0k6Z0dLjBh72iET3pPmW/536iCdy81adnlYpsEvp3ZvnfaQkG3Fz5XvEtw9AxQbbEBdrplC22BZT45bCOnG4LKwi60fwdbpdM/Zju06JLwFl0EPugh+wRkjqSIM1rejRxIzM+8GRX8bB5156yKdeY1HblSz/09U+yT45uSc5cTGt7UdjBO4AnGS9cs4t4ZggsHtFJe2SfH4S5b0UNl/xR7RFAzgSLNGxIYf537NRY/jm3oiht+r7d9nqs2F3O3WtUndlQxYJUApRJLEaMvv17N/k1IbprauesfUhLlT12FXEQIfG1GKcGCP0cAt4YftfGnReMc+fi4ykhtb64h/GfbZKp4jENNzU0jSWVLVjpVVvReVxuCUcNVU3TMmm5wCNx/B+lh7/eqbeLoq7C/BZIJh926FKBupwNxW79tJhW96FXIxPBjOGkN/lWSUI3OLHLrcGK9xz/2e8miLb+hjNySCVPj+4o6BXimKrjyA+meWiQ1eyu6Nybp910mpgi0TTIoHMKB9pGScGcB2gOIaekIOGoNOtwG7BmBpZ20AA4rLEL8olX8k+6R9Ept6NFDOBuq3KlQgLiibb6uVTF94aqzMlZXg8YXNTX/861ZKe9B9S7D1kh96zY2Amwx6PYAyeiXYXgJeSTTqdUkai4fKBixkR9ISRlBHU6jErh1kMGN5u47AuAulyiuViAD7Gfz1eu3CA0+ACaWRb8hyu396PHDEuzv+m5RK7Siyiw/fc4jzlT+J61lai3eR+fUAGqW+i/awXJYFIghoc1YbbrmSs/S0NP04aJKPNL1rhYZs/Rltn3ZVF4d3R4IAJk79fAy/nBwftTZYvZmaQ4rsOUSXXVkYFhnyT6fs5N4RYTfJBxg3O1xbUF75iDMmMLIi5f92wTzjfqTOCN3aHRTy6Y5ikCyACQa962J6XGu/AAro7tWcUzX4z6jDh/WdfO91xVvhV5ZWQw8PTRkpxDAEF6xTvhy2r6BS35RAN0CGS2+G8UOiHJYbcYncD+2zNSvpRcNkd5+51+CswBYMFpLJb4bxQ6IclhtxidwP7bM1K14VtoSCWtESf2EjMf8HTv3CCML8QViuUhJ6OTc7f+nJL8DUdHO4R//bnnyslWl+hnJs63DSV552rtYepOacyo4VvQRrosLfiDpfbOc8m/D3zRjXqkR3d9VOWxGqTYwUaBZ8VkmCyCPE52lRJ5X7oRiRQvtkv8BnqyZ+pdVW/cWBAdayNciMQcaABYQu9VNEKeZ2B7caByyRmfIX0M6FjNzJfrHrx4M9vO/ybgUIX/MCUGoYWMS4bFROUCZH2yl6UKwMVLNou60NCaZEyYKQG30jaxxl6lMFOzFolji13aqIKXx+bk27y/AWK3VcF3fR9QPhaxER98pgNR3Gi4GDJ0YlmVbZBNCfVmdgLC7rpF/JkixpcZkvXrXv2b6+WroIhpLixhTDn4nugzR1OFACsWUsvfzuUxlFba7j/KhMxKsyOjxNS/63Ngi5EY/XRepTf7Pdp6i6wm/RZf8jHI3vggIRdUSydEN8uQkqNG5DSWP2WNAAtLLKSzDDXaXyu6zL6lYXoWPBXPMUDln6qh6siRAVvLwg6eEF1Q3DxvvBILICf9Cuf0k7bFGQUP5jqsg2CPb0k+J4VIDyMSVGza9HgaSY58hgChnVLuc2cGPjY3Stmusqwwx1IJX0sAFhUto+uFRBrhKxOYXpejf5qN6a/WI9wHA58CN/IR22xBcpm8gK9SU0s98XVyC+ePmEmsQtsRtGwQOquDcttHIhaV3c7ktrSyHzE0J+ryC2CBeaAOZWf7N5Uhp684NvJs9ebxsL7Q=="
   :expiration "07/15/2018"})

(def professional-liability-payload-sample
  {"EndClientUserUniqueSessionId" "Uniquesession",
   "ServiceRequestDetail" { "OwnerId" "27",
                            "ResponseType" "JSON",
                            "UserName" "CWAgent",
                           "Token" (:token professional-liability-service)
                            #_
                            "LzOEfkD2G3fGNoAE/2OgpmZ6ZAWRLLmX3FtnciU1JdwB7qwRQYgWqXWRBQy4Jisd12JrkD8zgu8i1v6CrW16dewQ2Scv8nR12BrVp+lvGg0lxsyIucmAQER65neynmkI26Y5kbAKryCz/35ZriwlEfDXZLd3QYKRS9sc1TFkEt6yTSAaP1ldfyjTLdzzfEQFoh7wOZWhBG29a4ODeWgWKNqFuUXerFyeXwvtro+fUBYQXd5mtYN/lzWohbRL16+TihHcVST4ZQnE4Fonl8CEhQ6YFdByTdos0eHQxT/dz4891JftAgkDVq1rOak9nOy0qKAb6NoYtEXSPkZxG/nyasMGl47fu6XYWahaa/QIZENFrZ4dBGnlmva/Hik80iY7SwSCRT42VVmyhq6ysvWlyFyi4P+62OAQH7p5AoUM0n5fdi07EU6YqwSTUzWil4s3LplkIHDT7RPe+14gjvaPO1LxHFpvJF+c4svSQ7/R1nMH2HCR8o6y8vDoTDdkYe5HZ5EshW3rZCpHonG2vmGxPu2gVz/SRhNP5ODPqs88n13IswG5c0y1cYIWWoc/vdLvDUrSqf4AQQE31LyBlGGTg6uPCY4eYWCcrNtoQ2MK535MAkok7QvW9MFsv6mwc/nJvAxLBSe7e0DKvi8zbk+1Mgs4qUG8qi4auVL+Q4eWcLg+DjEsaLvz3ZXLy4wLs67p/XxzSCu8Hy5dsCae+kJI6uEmQZ0vMelDdEbeZY+Rh75YHy2vlSsHjhmiqWPDNieMgzoY/FaG87hEyGTYVv/HtjBnxOs8BCM+9gLOUHfF2gmw8tsymCLkx+abQ4oo4WUyIJp8iV+u1WjVLTwUijxis1AyM9D5pC4e+VYbeMpITktap/f/Fhb1FDwPq+F3MnhwMAUSjlPK+YknkoG69gA4YX3Ij5YMc82X5dVzsJR6cLEFkcue/0k6Z0dLjBh72iET3pPmW/536iCdy81adnlYpsEvp3ZvnfaQkG3Fz5XvEtw9AxQbbEBdrplC22BZT45bCOnG4LKwi60fwdbpdM/Zju06JLwFl0EPugh+wRkjqSIM1rejRxIzM+8GRX8bB5156yKdeY1HblSz/09U+yT45uSc5cTGt7UdjBO4AnGS9cs4t4ZggsHtFJe2SfH4S5b0UNl/xR7RFAzgSLNGxIYf537NRY/jm3oiht+r7d9nqs2F3O3WtUndlQxYJUApRJLEaMvv17N/k1IbprauesfUhLlT12FXEQIfG1GKcGCP0cAt4YftfGnReMc+fi4ykhtb64h/GfbZKp4jENNzU0jSWVLVjpVVvReVxuCUcNVU3TMmm5wCNx/B+lh7/eqbeLoq7C/BZIJh926FKBupwNxW79tJhW96FXIxPBjOGkN/lWSUI3OLHLrcGK9xz/2e8miLb+hjNySCVPj+4o6BXimKrjyA+meWiQ1eyu6Nybp910mpgi0TTIoHMKB9pGScGcB2gOIaekIOGoNOtwG7BmBpZ20AA4rLEL8olX8k+6R9Ept6NFDOBuq3KlQgLiibb6uVTF94aqzMlZXg8YXNTX/861ZKe9B9S7D1kh96zY2Amwx6PYAyeiXYXgJeSTTqdUkai4fKBixkR9ISRlBHU6jErh1kMGN5u47AuAulyiuViAD7Gfz1eu3CA0+ACaWRb8hyu396PHDEuzv+m5RK7Siyiw/fc4jzlT+J61lai3eR+fUAGqW+i/awXJYFIghoc1YbbrmSs/S0NP04aJKPNL1rhYZs/Rltn3ZVF4d3R4IAJk79fAy/nBwftTZYvZmaQ4rsOUSXXVkYFhnyT6fs5N4RYTfJBxg3O1xbUF75iDMmMLIi5f92wTzjfqTOCN3aHRTy6Y5ikCyACQa962J6XGu/AAro7tWcUzX4z6jDh/WdfO91xVvhV5ZWQw8PTRkpxDAEF6xTvhy2r6BS35RAN0CGS2+G8UOiHJYbcYncD+2zNSvpRcNkd5+51+CswBYMFpLJb4bxQ6IclhtxidwP7bM1K14VtoSCWtESf2EjMf8HTv3CCML8QViuUhJ6OTc7f+nJL8DUdHO4R//bnnyslWl+hnJs63DSV552rtYepOacyo4VvQRrosLfiDpfbOc8m/D3zRjXqkR3d9VOWxGqTYwUaBZ8VkmCyCPE52lRJ5X7oRiRQvtkv8BnqyZ+pdVW/cWBAdayNciMQcaABYQu9VNEKeZ2B7caByyRmfIX0M6FjNzJfrHrx4M9vO/ybgUIX/MCUGoYWMS4bFROUCZH2yl6UKwMVLNou60NCaZEyYKQG30jaxxl6lMFOzFolji13aqIKXx+bk27y/AWK3VcF3fR9QPhaxER98pgNR3Gi4GDJ0YlmVbZBNCfVmdgLC7rpF/JkixpcZkvXrXv2b6+WroIhpLixhTDn4nugzR1OFACsWUsvfzuUxlFba7j/KhMxKsyOjxNS/63Ngi5EY/XRepTf7Pdp6i6wm/RZf8jHI3vggIRdUSydEN8uQkqNG5DSWP2WNAAtLLKSzDDXaXyu6zL6lYXoWPBXPMUDln6qh6siRAVvLwg6eEF1Q3DxvvBILICf9Cuf0k7bFGQUP5jqsg2CPb0k+J4VIDyMSVGza9HgaSY58hgChnVLuc2cGPjY3Stmusqwwx1IJX0sAFhUto+uFRBrhKxOYXpejf5qN6a/WI9wHA58CN/IR22xBcpm8gK9SU0s98XVyC+ePmEmsQtsRtGwQOquDcttHIhaV3c7ktrSyHzE0J+ryC2CBeaAOZWf7N5Uhp684NvJs9ebxsL7Q==",
                            "BrowserIp" "127.0.0.1",
                            "ServiceRequestVersion" "1.0",
                            "RegionCode" "US"}
   "Policy" { "ProgramName" "Healthcare",
              "PolicyType" "Individual",
              "ZipCode" "35005",
              "State" "AL",
              "EffectiveDate" "2018-05-14",
              "EventName" "PLICalculateRate",
              "EventVersion" "2.2.2.1",
              "ApplicableFromDate" "2018-05-14",
              "NumOfProviders" "1",
              "SharedOrSeparateLimit" "Shared",
              "PLOrWLCoverageType" "Claims-Made",
              "ProfessionalLiabilityLimitsOfInsurance" "$1M/$3M",
              "WorkplaceLiabilityLimitsOfInsurance" "$2M/$6M",
              "WorkplaceLiabilityInsurance" "Yes",
              "CorporationOrPartnershipCoverage" "Yes",
              "AdditionalInsuredCoverage" "Yes",
              "EmploymentCategory" "Employed",
              "TransitionFactor" "10",
              "ProductNumber" "HC_PLI",
              "ProductVerNumber" "2.0",
              "ExpensesInsideOrOutside" "Inside",
              "DeductibleAmount" "500",
              "NumberOfClaimsinPast5Yrs" "2",
              "TotalReportedIncurredLossandExpense" "200000",
              "RetroDate" "Inception",
              "Revenue" "2000000",
              "ResidentialOrCommercialBusinessModifier" "10",
              "ProjectSizeModifier" "10",
              "WrittenContractsModifier" "10",
              "RiskManagementModifier" "10",
              "FinancialStrength" "10",
              "UseofSubcontractors" "10",
              "HistoryofDisciplinaryAction" "10",
              "MergersandAcquisitions" "10",
              "ExperienceofPrincipals" "10",
              "ClientSize" "100",
              "RequireSubstoShowProofOfEAndO" "10",
              "RequireAandEstoShowProofOfEandO" "10",
              "KYLocalGovernmentTaxRate" "20",
              "KYLocalGovernmentTaxOverridden" "No",
              "Provider" [
                           {
                            "ProviderDetail" { "FirstName" "John",
                                               "MiddleInitial" "E",
                                               "LastName" "Doe",
                                               "ProviderClass" "Athletic Trainer",
                                               "ProfessionalAssociation" "Licensed",
                                               "YearsOfPolicy" "1",
                                               "YearsInPractice" "1",
                                               "RiskManagementDiscount" "Yes",
                                               "PatientCompensationFund" "No",
                                               "HoursWorkPerWeek" "25",
                                               "ScheduleRatingFactor" "25",
                                               "ProviderEmailId" "john@gmail.com",
                                               "MeetsStudentQualification" "Yes",
                                               "UseofWrittenContractsFactor" "10",
                                               "TrainingorEducationFactor" "10",
                                               "ProfessionalReputationFactor" "10",
                                               "NatureofOperationsFactor" "10"}}]}

   "OwnerId" "27"})




#_
(chan/echo (fetch-json professional-liability-service
                       professional-liability-payload-sample))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RENTERS

(def renters-service
  {:endpoint "https://rentersapihk.solartis.net/KnowledgeEngineV6_2/KnowledgeBase/FireEventV2"
   :access #{:rate :policy-pdf}
   :headers {"EventName" (case :calc
                           :calc "Renters_CalculateRate")}
   :token
   (encode-token
    "v/hp2l6cEGpkSKz1cMqPzRcL2h/w5px8qVY4FYnOun591GywCfVXyf49jaqNrSV7uKW9pTCDtqTgXrVou
wghAq4TfKGTDyS1nEQFRg7mIczanoJGqdV43LxrPK92f7aqzAWUJjM9mClRUVNIxF4Y8VjiCRMk0pPAP
piPvoLAKNfe7pCDPDfE/ZqK072BhiJZ/WG2abOIAva236YVjgBzF976Zi7FcYehQCfqYbPHPtBD2U/ikvWc
Q779HOOSMGDn0HO1y3AJ76Ad1gBFIkyMFk8o2alAlHB6CXx2HJBZ2YeygTUugsBPbHCtP/xmyXRy1ba
WIXXJrQe16QpmkxalnfK7ArzG4SlUymx0u4nLd94pCXwLF8jmKnqAh/RqJB2Q5txNkV5FYv450m+2Pgw
fswH+4iMrtPdduhBYLeSqiiORYsV+Nvqm4DtYMhWNce7eeAvYlsoFywLAEkm4WqIshP2aqSt9nCT13y2
ouyNFjqiVN7khEyQWS7AkZDqJQnvVWAWZXQrklkj1JmnQusQjOb841ZVyzLOCdDtEFFSgcLy4gMNsAA
DonLn4/a+Karoaqnpj+AKhczvrkw+6Mc6PhsrsSeTy7hMFeAwQhGsbR1RQOpOjr/SDfEZHAAht+PleLUV
xUJ9o1ojZqQ3WU4Jd0etHYCZPFBIkDLBCbBEizRFzmMvccdDEr2ptj7vEmCqxlmfjFg9GZtaR8fsvWvVt4/
/BS2zQaxyTJyW8l2/ZNb/5IMq57Ga0x2O/kvy2aFHAeICFPXXrKQLn8bJiyLUZCkbj/0yYiWf/l8tEpLV8yblV
+cTBEfsekyCqbAS713bK0fV0Tj8izzZ1rJlWyra4IWtKy5DvH03M2rvURTvGpjc+SvG6XPgDjjZD4MNt2Bh1
aBW3cSpnEex+zS+OyLuSOFYfHv1jnFNUudIcXruW/c09MJ0V/eEXugLW+bOMNCfPdVvTtdgjiWu5FwIy
URwmTpx8Nfr0Qe8+PrmzfJf8XJ3e0awAzyEGvBf4L/B1UTa6QC6KRpdNYXzhHQsvjsAjd501p4th9wHpC
dNNkzuiFCYe7vvZnkJ5OyoEp3mlhjgj")
   :expiration "07/15/2018"})

(def renters-payload-sample
  {"EndClientUserUniqueSessionId" "Uniquesession",
   "RentersProperty" { "EventName" "Renters_RateAndPDFGeneration",
                       "Environment" "PROD",
                       "ProductName" "Renters Rater V1",
                       "Carrier" "Solartis - Renters Program",
                       "PolicyEffectiveDate" "03/01/2018",
                       "PolicyExpirationDate" "03/01/2019",
                       "QuoteType" "NEW_BUSINESS",
                       "InsuredName" "John",
                       "AddressLine1" "4740 Reed Road",
                       "AddressLine2" "Suite 109",
                       "City" "Upper Arlington",
                       "County" "Franklin",
                       "StateCode" "OH",
                       "ZipCode" "43220",
                       "ServiceRequestVersion" "1.0",
                       "OwnerId" "33",
                       "UserName" "uwmanager",
                       "EnableValidation" "Y",
                      "Property" [{"PropertyDetail" { "ZipCode" "43220",
                                                      "CustomerBirthday" "5/23/1994",
                                                      "NoOfUnits" "1",
                                                      "ProtectionClass" "2",
                                                      "ConstructionType" "Poured Concrete",
                                                      "CreditScore" "721-725",
                                                      "PriorLosses" "Unknown",
                                                      "SeverityPriorLosses" "3999",
                                                      "LossesWithLM" "2",
                                                      "PersonalPropertyLimit" "10000",
                                                      "PersonalLiabiltyLimit" "100000",
                                                      "MedicalPaymentLimit" "1000",
                                                      "Deductible" "500",
                                                      "AddressLine1" "4740 Reed Road",
                                                      "AddressLine2" "Suite 109",
                                                      "City" "Upper Arlington",
                                                      "County" "Franklin",
                                                      "StateCode" "OH"}}]}
   "OwnerId" "33",
   "ServiceRequestDetail" { "ResponseType" "JSON",
                            "OwnerId" "33",
                            "UserName" "uwmanager",
                            "Token" (:token renters-service)
                            #_
                            "VLOouMoPSJR1WT5DF489lo+U19gRsGs6p9dSX8+actIVVH2tXLPiA53dz002/lITGTZvoNr0rpDsMrJPTOO5fk2KhDcb2FbtZOqxGESpQqFV1FN2lxvwbe/H8vHkOrCKOkOpHdJm/J4pllWgNa6jWXGZDVHCWzOwXKGCVssQM81CEoQBSVQGZYzXR1amm50lLplkIHDT7RPe+14gjvaPO1LxHFpvJF+c4svSQ7/R1nMH2HCR8o6y8vDoTDdkYe5HZ5EshW3rZCpHonG2vmGxPu2gVz/SRhNP5ODPqs88n13IswG5c0y1cYIWWoc/vdLvDUrSqf4AQQE31LyBlGGTg6uPCY4eYWCcrNtoQ2MK5364UpGP5paRHJ5oFDQ5XABqky5c+WgnIlXW2bTWejQfCzjc+L5gNhfm8EYjot10PvhYYRSqk9/rEpdMhUeu2hsGwk0nIiloccvspNV0WHM4s54t8h8VHiDqlVDglEYuqQvoEoq+1PjrtE/qtia2/bSJ0GDl0pVafvC88hWUkdE2873/gVtGR6m4PHGlIgrLrZCQZaRQqbBg4UAMeqJBBtYNf4fjTq+c2aQKHd3qufejMJSI8yJKK8QGX3CO5x4LO9k9XkKL1SGjqeYrTmDhVl//Ed2LVBysLR/yPKRneWR5BUyN7pGtXZUexlQLKC3FlEGfXgtcb9pJ0EcnE0vcfrMhso8DQvuuONAB2pJqOvmKSXc0qYiqYBHzwpMM5sijrpIEIzR/PBHaQwu5BR63dvFz26MFnUHO7H7ZMd/oPQeOlmtKy5DvH03M2rvURTvGpjdcphzuSmL8g8WsqyGKKIdycLk54H+BI+6UURrLi5Wk2LdUBS8eXazSrDIS5DcfPtzkhWEmryuyMT7xoGX18+AF+SDKuexmtMdjv5L8tmhRwPXj7vAXTdCToARTduzXExD+u+LXDF7/neunxsq7/V8/DXeyepuNSO8SPAqVbRdcJgWZbEN9BJdPHr05nn7OTSc2IrMP1J3lbhFoDS79cbbuFQP60uSRJ5K5yKKPuoYqtfYmXbPwHfRNwdqi7wCN3xw=",
                            "BrowserIp" "127.0.0.1" #_"111.93.250.10",
                            "ServiceRequestVersion" "1.0"}})

(defn fetch-renters [& [custom]]
  (fetch-json renters-service
              (deep-merge
               renters-payload-sample
               (solartis-keys custom))))

#_
(chan/echo (fetch-renters {}))

#_
(chan/echo (fetch-renters {:renters-property {:property {0 {}}}}))

#_
(chan/echo (fetch-renters {:renters-property
                                {:property
                                 {0
                                  {:property-detail
                                   {:zip-code "43220"}}}}}))
