(ns server.proxy
  (:require-macros
   [cljs.core.async.macros :as m
    :refer [go go-loop alt!]])
  (:require
   [cljs.nodejs :as nodejs]
   [cljs.core.async :as async
    :refer [chan close! timeout put!]]
   [taoensso.timbre :as timbre]
   [mount.core :as mount
    :refer [defstate]]
   [camel-snake-kebab.extras
    :refer [transform-keys]]
   [macchiato.util.response :as r]
   [sdk.solartis :as solartis]))

(def default-renters-property
  {:property
    {0
     {:property-detail
      {:zip-code "43220"}}}})

(defn ->cljs-key [a]
  ;; respecting integers as keys
  (if (string? a)
    (let [n (js/parseInt a)]
      (if-not (integer? n)
        (keyword a)
        n))
    a))

(defn handler [{:keys [body] :as req} res]
  ;; ## body should be auto parsed by middleware
  (timbre/debug "Solartis handler:" req)
  (let [content (->> (js->clj (.-body body))
                     (transform-keys ->cljs-key))]
    (timbre/debug "Solartis -> " content)
    (go-loop [data (<! (solartis/fetch-renters
                        {:renters-property
                         (or content
                             default-renters-property)}))]
      (timbre/debug "=>" data)
      (if (:success data)
        (-> (r/ok (:body data))
            (r/content-type "application/json")
            (res))
        (-> (r/not-found "Failed")
            (res))))))
